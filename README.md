## Subscription management platform

This is my thesis project. It's a platform for managing subscriptions, it gathers data about users and subscriptions and creates visualizations to make decision making easier.
API documentation (outdated right now, I'm rewriting parts of the project at the moment and will hopefully update the docs afterwards) is here: http://docs.smp2.apiary.io/

#### So, what can it do?

It's intended for developers of various apps, web services, blogs etc. It takes care of some of the bookkeeping for you, mainly information about which users are allowed to use certain features, view certain content and so on. It logs usage information and processes it, so that it's easy to understand and explore usage patterns.

The back-end is written in Go and uses MySQL as the underlying database (another DB could be used instead, but that would require rewriting a bit of code). The website is made using Vue.js. It's possible to write a 3rd party front-end, to replace the one I wrote, using the API documentation (http://docs.smp2.apiary.io/).

You can read the **accompanying paper** if you are interested in the reasons behind some of my decisions or other details. [It's available here.](paper/smp-paper.pdf)

Note that this project is a work in progress, **is not stable** and things will most certainly break in the future.

#### The service can handle multiple apps, so you can host it on your server and let your friends use it, etc.
![login_picture](img/smp_login.png)
![menu_picture](img/smp_menu.png)

#### You can set up new subscription plans. They will be used to generate subscriptions for new users of your app or service.
![plan_picture](img/smp_plan.png)
#### You can also change the plans that already exist.
![plan2_picture](img/smp_plan2.png)
![plan3_picture](img/smp_plan3.png)

#### Create discounts.
![discount_picture](img/smp_discount.png)

#### Generate discount codes (e.g. to be sent out automatically by your other tools).
![codes_picture](img/smp_codes.png)
![codes2_picture](img/smp_codes2.png)
![codes3_picture](img/smp_codes3.png)

#### You can create accounts for other people to access these settings.

#### And of course, view the data.
![charts_picture](img/smp_charts.png)

#### For now, there's just a few different charts.
![new_picture](img/smp_new.png)

#### I tried to include the most useful ones first.
![attrition_picture](img/smp_attrition.png)
![revenue_picture](img/smp_revenue.png)

#### Is your free trial convincing users to buy things?
![conversions_picture](img/smp_conversions.png)

#### Are 'whales' keeping your app alive?
![revpay_picture](img/smp_revpay.png)
![revpay2_picture](img/smp_revpay2.png)

#### How many users with different subscriptions do you have?
![subs_picture](img/smp_subs.png)

#### How effective was your marketing campaign, aren't those new users just leaving after a few days?
![tag_percentage_picture](img/smp_tag_percentage.png)

package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/julienschmidt/httprouter"

	"encoding/hex"
	"encoding/json"

	"time"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var dbCredentials = "xpospe03:unga4asu@unix(/var/run/mysql/mysql.sock)/xpospe03"

type serverConfig struct {
	lastRun time.Time
}

var config serverConfig

var errMoreSubs = errors.New("multiple active subscriptions")
var errNoSubs = errors.New("no active subscriptions")

var errMorePlans = errors.New("multiple active plans")
var errNoPlans = errors.New("no active plans")

//map that holds mutexes for plans, and subscriptions of individual users
//it should always be accessed through the RWMutex associated with it
var muMap = make(map[string]*sync.RWMutex)
var muMapMutex = &sync.RWMutex{} //mutex to handle concurrent operations on the muMap

func main() {
	fmt.Println("Hello, Gopher!")
	var err error
	db, err = sql.Open("mysql", dbCredentials)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		// do something here
		log.Fatal(err)
	}
	fmt.Println("db pinged")

	//running tests
	// testDB()
	// return

	// defining routes
	router := httprouter.New()
	//automatically reply to OPTIONS requests with 200 OK and Allow header informing the client which methods are supported
	// router.HandleOPTIONS = true
	router.PanicHandler = HandlePanics
	router.RedirectTrailingSlash = true

	router.GET("/api/v0/test/counts", RequestsTestCounts)
	router.GET("/api/v0/test/portions", RequestsTestTypes)
	router.GET("/api/v0/test/convs", RequestsTestConvs)

	router.GET("/api/v0/app/:appname/login/:login", SanitizeParams(LoginHandler))

	router.GET("/api/v0/app/:appname", SanitizeParams(AuthApp(GetApp)))
	router.PUT("/api/v0/app/:appname", SanitizeParams(AuthRegistration(CreateApp)))
	router.PATCH("/api/v0/app/:appname", SanitizeParams(AuthApp(ModifyApp)))
	router.DELETE("/api/v0/app/:appname", SanitizeParams(AuthApp(DeleteApp)))

	router.GET("/api/v0/app/:appname/admin/:login", SanitizeParams(AuthApp(GetAdmin)))
	router.PUT("/api/v0/app/:appname/admin/:login", SanitizeParams(AuthApp(CreateAdmin)))
	router.PATCH("/api/v0/app/:appname/admin/:login", SanitizeParams(AuthApp(ModifyAdmin)))
	router.DELETE("/api/v0/app/:appname/admin/:login", SanitizeParams(AuthApp(DeleteAdmin)))

	router.GET("/api/v0/app/:appname/plan/:planname", SanitizeParams(AuthApp(PlanGuard(GetPlan))))
	router.PUT("/api/v0/app/:appname/plan/:planname", SanitizeParams(AuthApp(PlanGuard(CreatePlan))))
	router.PATCH("/api/v0/app/:appname/plan/:planname", SanitizeParams(AuthApp(PlanGuard(ModifyPlan))))
	router.DELETE("/api/v0/app/:appname/plan/:planname", SanitizeParams(AuthApp(PlanGuard(DeletePlan))))
	router.GET("/api/v0/app/:appname/plan", SanitizeParams(AuthApp(PlanGuard(GetActivePlan))))
	router.GET("/api/v0/app/:appname/plans", SanitizeParams(AuthApp(PlanGuard(GetAllPlans))))

	router.GET("/api/v0/app/:appname/user/:username", SanitizeParams(AuthApp(GetUser)))
	router.PUT("/api/v0/app/:appname/user/:username", SanitizeParams(AuthApp(CreateUser)))
	router.PATCH("/api/v0/app/:appname/user/:username", SanitizeParams(AuthApp(ModifyUser)))
	router.DELETE("/api/v0/app/:appname/user/:username", SanitizeParams(AuthApp(DeleteUser)))
	router.GET("/api/v0/app/:appname/users", SanitizeParams(AuthApp(GetAllUsers)))

	router.GET("/api/v0/app/:appname/user/:username/sub/:subid", SanitizeParams(AuthUser(SubGuard(GetSub))))
	router.POST("/api/v0/app/:appname/user/:username/sub", SanitizeParams(AuthApp(SubGuard(CreateSub))))
	router.PATCH("/api/v0/app/:appname/user/:username/sub/:subid", SanitizeParams(AuthUser(SubGuard(ModifySub))))
	//no reason to delete subs
	router.GET("/api/v0/app/:appname/user/:username/sub", SanitizeParams(AuthApp(SubGuard(GetActiveSub))))
	router.PATCH("/api/v0/app/:appname/user/:username/sub", SanitizeParams(AuthApp(SubGuard(ModifyActiveSub))))
	router.GET("/api/v0/app/:appname/user/:username/subs", SanitizeParams(AuthUser(SubGuard(GetAllSubs))))

	router.GET("/api/v0/app/:appname/user/:username/sub/:subid/ts", SanitizeParams(AuthApp(GetTimestamps)))

	router.GET("/api/v0/app/:appname/plan/:planname/discount/:discountname", SanitizeParams(AuthApp(GetDiscount)))
	router.PUT("/api/v0/app/:appname/plan/:planname/discount/:discountname", SanitizeParams(AuthApp(CreateDiscount)))
	router.PATCH("/api/v0/app/:appname/plan/:planname/discount/:discountname", SanitizeParams(AuthApp(ModifyDiscount)))
	router.DELETE("/api/v0/app/:appname/plan/:planname/discount/:discountname", SanitizeParams(AuthApp(DeleteDiscount)))
	router.GET("/api/v0/app/:appname/plan/:planname/discounts", SanitizeParams(AuthApp(GetPlanDiscounts)))
	router.GET("/api/v0/app/:appname/discounts", SanitizeParams(AuthApp(GetAppDiscounts)))

	router.POST("/api/v0/app/:appname/plan/:planname/discount/:discountname/codes", SanitizeParams(AuthApp(GenerateDiscountCodes)))
	router.GET("/api/v0/app/:appname/code/:code", SanitizeParams(AuthUser(CheckDiscountCode)))
	router.DELETE("/api/v0/app/:appname/code/:code", SanitizeParams(AuthApp(DeleteDiscountCode)))

	router.GET("/api/v0/app/:appname/plan/:planname/tags", SanitizeParams(AuthApp(GetTags)))
	router.GET("/api/v0/app/:appname/plan/:planname/tags/:tag", SanitizeParams(AuthApp(CheckTag)))
	router.PUT("/api/v0/app/:appname/plan/:planname/tags/:tag", SanitizeParams(AuthApp(CreateTag)))
	router.DELETE("/api/v0/app/:appname/plan/:planname/tags/:tag", SanitizeParams(AuthApp(DeleteTag)))
	router.GET("/api/v0/app/:appname/user/:username/sub/:subid/tags", SanitizeParams(AuthApp(GetTags)))
	router.GET("/api/v0/app/:appname/user/:username/sub/:subid/tags/:tag", SanitizeParams(AuthApp(CheckTag)))
	router.PUT("/api/v0/app/:appname/user/:username/sub/:subid/tags/:tag", SanitizeParams(AuthApp(CreateTag)))
	router.DELETE("/api/v0/app/:appname/user/:username/sub/:subid/tags/:tag", SanitizeParams(AuthApp(DeleteTag)))

	router.OPTIONS("/api/v0/app/:appname", OptionsHeaders(OptionsGPuPD))
	router.OPTIONS("/api/v0/app/:appname/admin/:login", OptionsHeaders(OptionsGPuPD))
	router.OPTIONS("/api/v0/app/:appname/plan/:planname", OptionsHeaders(OptionsGPuPD))
	router.OPTIONS("/api/v0/app/:appname/plan", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/plans", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/user/:username", OptionsHeaders(OptionsGPuPD))
	router.OPTIONS("/api/v0/app/:appname/users", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/user/:username/sub", OptionsHeaders(OptionsGPoP))
	router.OPTIONS("/api/v0/app/:appname/user/:username/sub/:subid", OptionsHeaders(OptionsGP))
	router.OPTIONS("/api/v0/app/:appname/user/:username/subs", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/user/:username/sub/:subid/ts", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/plan/:planname/discount/:discountname", OptionsHeaders(OptionsGPuPD))
	router.OPTIONS("/api/v0/app/:appname/plan/:planname/discounts", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/discounts", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/plan/:planname/discount/:discountname/codes", OptionsHeaders(OptionsPo))
	router.OPTIONS("/api/v0/app/:appname/code/:code", OptionsHeaders(OptionsGD))
	router.OPTIONS("/api/v0/app/:appname/plan/:planname/tags/:tag", OptionsHeaders(OptionsGPuD))
	router.OPTIONS("/api/v0/app/:appname/plan/:planname/tags", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/app/:appname/user/:username/sub/:subid/tags/:tag", OptionsHeaders(OptionsGPuD))
	router.OPTIONS("/api/v0/app/:appname/user/:username/sub/:subid/tags", OptionsHeaders(OptionsG))

	//stats
	router.GET("/api/v0/stats/:appname/users/new", SanitizeQueryParams(AuthApp(GetNewUsers)))
	router.GET("/api/v0/stats/:appname/users/active", SanitizeQueryParams(AuthApp(GetActiveUsers)))
	router.GET("/api/v0/stats/:appname/users/attrition", SanitizeQueryParams(AuthApp(GetLostUsers)))
	router.GET("/api/v0/stats/:appname/users/paying", SanitizeQueryParams(AuthApp(GetPayingUsers)))
	router.GET("/api/v0/stats/:appname/subs/new", SanitizeQueryParams(AuthApp(GetNewSubs)))
	router.GET("/api/v0/stats/:appname/subs/comp", SanitizeQueryParams(AuthApp(GetAdoptionSubs)))
	router.GET("/api/v0/stats/:appname/subs/conv", SanitizeQueryParams(AuthApp(GetConversions)))
	router.GET("/api/v0/stats/:appname/plans/comp", SanitizeQueryParams(AuthApp(GetAdoptionPlans)))
	router.GET("/api/v0/stats/:appname/rev/user", SanitizeQueryParams(AuthApp(GetRevenue)))
	router.GET("/api/v0/stats/:appname/rev/paying", SanitizeQueryParams(AuthApp(GetRevenuePaying)))
	router.GET("/api/v0/stats/:appname/tags/:tag", SanitizeQueryParams(AuthApp(GetTagPrevalence)))

	router.OPTIONS("/api/v0/stats/:appname/users/new", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/users/active", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/users/attrition", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/users/paying", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/subs/new", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/subs/comp", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/subs/conv", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/plans/comp", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/rev/user", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/rev/paying", OptionsHeaders(OptionsG))
	router.OPTIONS("/api/v0/stats/:appname/tags/:tag", OptionsHeaders(OptionsG))

	router.OPTIONS("/api/v0/app/:appname/login/:login", OptionsHeaders(OptionsG))

	// routes for resources and html files
	//router.GET("/resources/*resourcepath", Resources)
	//router.GET("/static/*htmlpath", Show)

	//router.POST("/api/admin/search")

	//router.GET("/protected/*htmlpath", protector(ShowProtected))

	//router.POST("/api/login", auth)
	//router.POST("/login", Login)
	//router.GET("/", Index)

	// fs := http.FileServer(http.Dir(""))
	// http.Handle("/", fs)

	//check lastRun, if it's too long ago, run the maintenance functions and advance time by an hour until caught up
	// if time.Now().Sub(config.lastRun) > time.Hour {
	// 	go renewCron(config.lastRun)
	// } else {
	// 	go renewCron(time.Unix(0, 0))
	// }

	fmt.Println("Listening & serving.")
	log.Fatal(http.ListenAndServe(":23456", router))
}

//PlanGuard takes care of concurrent accesses to plan resources.
//It doesn't let any request proceed to its handler as long a non-GET request is being handled for any plan resource.
func PlanGuard(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		// defer func() {
		// 	//debugging function
		// 	muMapMutex.RLock()
		// 	fmt.Println("Mutex map size is:\t", len(muMap))
		// 	fmt.Printf("%#v\n", muMap)
		// 	muMapMutex.RUnlock()
		// }()
		appname := ps.ByName("appname")

		//if not already present in the map, create a mutex and put it there
		//locking the map for reading
		muMapMutex.RLock()
		mu, ok := muMap[appname+"_plan"]
		muMapMutex.RUnlock()
		if !ok {
			//locking the map for writing
			muMapMutex.Lock()
			//check again, it's possible that the map item was created in the meantime
			if _, ok := muMap[appname+"_plan"]; !ok {
				mu = &sync.RWMutex{}
				muMap[appname+"_plan"] = mu
			}
			muMapMutex.Unlock()
		}

		if r.Method != "GET" {
			mu.Lock()
			defer mu.Unlock()
		} else {
			mu.RLock()
			defer mu.RUnlock()
		}
		//zero value of a mutex is an unlocked mutex but the map is of pointers to mutex, not mutexes
		//so.. the map gets bigger and bigger
		//not a problem with plans (not enough of them to be a problem), but it's going to be a problem with users' subs
		//need to rethink this using channels, I guess
		//works as a temporary workaround, to get around the map growth we can just delete and recreate it every X minutes (crappy solutions ftw)
		protected(w, r, ps)
	})
}

//SubGuard takes care of concurrent accesses to subscription resources of an individual user.
//It doesn't let any request proceed to its handler as long a non-GET request is being handled (for the same user).
func SubGuard(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		appname := ps.ByName("appname")
		username := ps.ByName("username")

		//if not already present in the map, create a mutex and put it there
		muMapMutex.RLock()
		mu, ok := muMap[appname+"_sub_"+username]
		muMapMutex.RUnlock()
		if !ok {
			//locking the map for writing
			muMapMutex.Lock()
			//check again, it's possible that the map item was created in the meantime
			if _, ok := muMap[appname+"_sub_"+username]; !ok {
				mu = &sync.RWMutex{}
				muMap[appname+"_sub_"+username] = mu
			}
			muMapMutex.Unlock()
		}

		if r.Method != "GET" {
			mu.Lock()
			defer mu.Unlock()
		} else {
			mu.RLock()
			defer mu.RUnlock()
		}
		protected(w, r, ps)
	})
}

func LoginHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	appname := ps.ByName("appname")
	login := ps.ByName("login")
	//select admin entry from db
	//hash associated password with When header and compare
	app, err := selectApp(appname)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		fmt.Println("no such app")
		notAuthorized(w, r)
		return
	}
	admin, err := selectAdmin(appname+"Admins", login)
	if err != nil {
		if err != sql.ErrNoRows {
			panic(err)
		}
		fmt.Println("no such login, replying with 401")
		notAuthorized(w, r)
		return
	}
	h, err := hex.DecodeString(r.Header.Get("Authorization"))
	if err != nil {
		//invalid Authorization, has to be a hex string
		fmt.Println(err)
		http.Error(w, "400 illegal characters in Authorization field", http.StatusBadRequest)
		return
	}
	when := r.Header.Get("When")
	hasher := hmac.New(sha256.New, []byte(admin.Password))
	hasher.Write([]byte(when))
	hash := hasher.Sum(nil)
	fmt.Printf("request hash: %x\n", h)
	fmt.Printf("generated hash: %x\n", h)
	if !hmac.Equal(h, hash) {
		fmt.Println("hashes don't match, replying with 401")
		notAuthorized(w, r)
		return
	}
	//hashes match, replying with app hash to use as an api key
	resp, err := json.Marshal(&app.Hash)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, "text")
	written, err := w.Write(resp)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s resource found and %d bytes written in response to %s request.\n", "Admin", written, "login")
}

func OptionsHeaders(protected httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, When")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		protected(w, r, ps)
	})
}

func OptionsG(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
}

func OptionsPo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
}

func OptionsGD(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, DELETE, OPTIONS")
}

func OptionsGPoP(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, OPTIONS")
}

func OptionsGP(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, PATCH, OPTIONS")
}

func OptionsGPD(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, PATCH, DELETE, OPTIONS")
}

func OptionsGPuD(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, DELETE, OPTIONS")
}

func OptionsGPuPD(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, PATCH, DELETE, OPTIONS")
}

//value of zero in current indicates that the function is running normally
//otherwise it's running in catch-up mode and will increase the time by hour and run until caught up
func renewCron(current time.Time) {
	var catchingUp bool
	if current.Unix() != 0 {
		catchingUp = true
		current = time.Now()
	}
	config.lastRun = current
	//run renew subs for all apps
	rows, err := db.Query(fmt.Sprintf("SELECT appname FROM `Apps`"))
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var apps []string
	for rows.Next() {
		//just copy to an array, so we can close the connection
		var appname string
		err = rows.Scan(&appname)
		if err != nil {
			log.Fatal(err)
		}
		apps = append(apps, appname)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	//run renewSubs for all apps
	for _, a := range apps {
		err = renewSubs(a, current)
		if err != nil {
			log.Fatal(err)
		}
	}
	//schedule the next run
	if catchingUp {
		//possibly, we could advance time by bigger leaps e.g. 6 or 12 hours, since we are looking 24 hours ahead
		go renewCron(current.Add(time.Hour))
		return
	}
	go time.AfterFunc(time.Hour, func() {
		go renewCron(time.Unix(0, 0))
	})
}

func HandlePanics(w http.ResponseWriter, r *http.Request, p interface{}) {
	fmt.Printf("%#v\n", p)
	if err, ok := p.(error); ok {
		if err == sql.ErrNoRows {
			fmt.Println("DB inconsistencies encountered, replying with a 500.")
			http.Error(w, "database error", http.StatusInternalServerError)
			return
		}
		//TODO create new error types to distinguish between db errors, json errors, strconv errors, etc.
		//reply with 500
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	fmt.Println(p)
	return
}

func RequestsTestCounts(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	myArray := [5]int{1, 2, 3, 4, 5}
	resp, err := json.Marshal(&myArray)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Testing data sent. Written bytes:", written)
}

func RequestsTestTypes(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	mySubTypes := map[string]int{
		"none":   1,
		"silver": 2,
		"bronze": 3,
		"gold":   4,
	}
	resp, err := json.Marshal(&mySubTypes)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Testing data sent. Written bytes:", written)
}

func RequestsTestConvs(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	myConversionRates := map[string]map[string]int{
		"free": {
			"none":   1,
			"silver": 2,
			"bronze": 3,
			"gold":   4,
		},
		"silver": {
			"free":   2,
			"gold":   3,
			"bronze": 4,
		},
		"gold": {
			"free":   3,
			"silver": 2,
			"none":   1,
		},
	}
	resp, err := json.Marshal(&myConversionRates)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set(contentType, appJSON)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	written, err := w.Write(resp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Testing data sent. Written bytes:", written)
}

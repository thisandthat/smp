FORMAT: 1A
# SMP
(outdated)
SMP is an API for Subscription Management Platform, it collects usage information of your service and offers aggregated data.

# Group Data Collection
Resources related to collecting data.

## App [/api/v0/{appname}]
### List app information [GET]
+ Parameters
    + appname: testApp (string, required)

+ Response 200 (application/json)

        {
            "appname":"testApp",
            "secret":"1234abcd",
            "hash":"magictesthash",
            "created":1485385200000
        }

### Register an app [PUT]
TODO use a special auth hash to limit usage of this endpoint only to the web interface.
This creates a new app resource under the supplied appname.
JSON object containing at least the appname and secret must be sent with the request.
Returns JSON representation of the resource on success, in case of a naming conflict, 409 "Conflict" is returned.
+ Parameters
    + appname: testApp (string, required)

+ Request (application/json)

        {
            "appname":"testApp",
            "secret":"1234abcd",
            "hash":"magictesthash",
            "created":1485385200000
        }

+ Response 201 (application/json)

        {
            "appname":"testApp",
            "secret":"1234abcd",
            "hash":"magictesthash",
            "created":1485385200000
        }

+ Response 409

        App name must be unique. Choose a different name.

### Modify app information [PATCH]
This modifies the resource associated with appname.
JSON with fields to be changed needs to be supplied, fields with no changes are not required.
The hash field cannot be changed (it is derived from appname and secret automatically).
Returns JSON representation of the resulting resource, or a 404 "Not Found" when trying to modify a nonexistant resource.
+ Parameters
    + appname: testApp (string, required)

+ Request (application/merge-patch+json)

        {   
            "secret":"newPassRotation"
        }

+ Response 200 (application/json)

        {
            "appname":"testApp",
            "secret":"newPassRotation",
            "hash":"newTestHash",
            "created":1485385200000
        }

### Delete an app [DELETE]
This destroys the resource, and all sub-resources
TODO use a special auth hash to limit usage of this endpoint only to the web interface.
+ Parameters
    + appname: testApp (string, required)

+ Response 204

## Admin [/api/v0/{appname}/admin/{login}]
Admins are accounts allowed to access the application configuration, data sets, etc.
This is used mainly to log in to the frontend web app.
### List admin information [GET]
+ Parameters
    + appname: testApp (string, required)
    + login: joe123

+ Response 200 (application/json)

        {
            "login":"joe123",
            "password":"supersecret987",
            "email":"joe@company.com"
        }

### Create an admin [PUT]
This creates a new admin resource, returns 409 "Conflict" on naming conflict.
+ Parameters
    + appname: testApp (string, required)
    + login: bob2

+ Request (application/json)

        {
            "login":"bob2",
            "password":"1234qwer",
            "email":"bob@company.com"
        }

+ Response 201 (application/json)

        {
            "login":"bob2",
            "password":"1234qwer",
            "email":"bob@company.com"
        }

+ Response 409

        Login must be unique. Choose a different login.


### Modify admin information [PATCH]
This modifies the resource associated with login.
JSON with fields to be changed needs to be supplied, fields with no changes are not required.
+ Parameters
    + appname: testApp (string, required)
    + login: joe123

+ Request (application/merge-patch+json)

        {
            "password":"iamveryoriginal"
        }

+ Response 200 (application/json)

        {
            "login":"joe123",
            "password":"iamveryoriginal",
            "email":"joe@company.com"
        }

### Delete admin resource associated with login [DELETE]
+ Parameters
    + appname: testApp (string, required)
    + login: joe123

+ Response 204

## Plan [/api/v0/{appname}/plan/{planid}]
User subscriptions are instances of app plans. Use the plan resources to define different subscription plans.
### List plan information [GET]
GET request URI can use either planid or name.
+ Parameters
    + appname: testApp (string, required)
    + planid: 1 (number or string, required)

+ Response 200 (application/json)

        {
            "planid":1,
            "name":"Free",
            "level":"free",
            "price":0,
            "length":30
        }

### Create a new plan [PUT /api/v0/{appname}/plan]
NB: the URI for this endpoint doesn't contain planid.
Creates a new plan, returns 409 "Conflict" on naming conflict (to avoid confusion only, for differentiating resources, planid is used).
+ Parameters
    + appname: testApp (string, required)

+ Request (application/json)

        {
            "name":"Lifetime Gold",
            "level":"gold",
            "price":60,
            "length":0
        }

+ Response 201 (application/json)
    
    + Headers
    
            Location: /api/v0/{appname}/plan/2
    + Body

            {
                "planid":2,
                "name":"Lifetime Gold",
                "level":"gold",
                "price":60,
                "length":0

            }

+ Response 409

        Choose a unique plan name.

### Modify a plan [PATCH]
+ Parameters
    + appname: testApp (string, required)
    + planid: 2

+ Request (application/merge-patch+json)

        {
            "price":30,
            "tags":["anniversary5"]
        }

+ Response 200 (application/json)

        {
            "planid":2,
            "name":"Lifetime Gold",
            "level":"gold",
            "price":30,
            "tags":["anniversary5"],
            "length":0
        }

### Delete a plan [DELETE]
+ Parameters
    + appname: testApp (string, required)
    + planid: 1

+ Response 204

## Default plan [/api/v0/{appname}/plan]
For convenience.
### List default plan information [GET]
+ Parameters
    + appname: testApp (string, required)

+ Response 200 (application/json)

        {
            "planid":1,
            "name":"Free",
            "level":"free",
            "price":0,
            "length":30
        }

## User [/api/v0/{appname}/user/{username}]
### List user information [GET]
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)

+ Response 200 (application/json)

        {
            "username":"user1",
            "hash":"user1hash",
            "email":"user1@email.com",
            "created":1485385200000,
            "retired":false
        }

### Create a user [PUT]
This creates a user resource. If it exists, it is replaced.
Returns JSON representation of the resource on success.
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)

+ Request (application/json)

        {
            "username":"user1",
            "hash":"user1hash",
            "email":"user1@email.com",
            "retired":false
        }

+ Response 201 (application/json)

        {
            "username":"user1",
            "hash":"user1hash",
            "email":"user1@email.com",
            "retired":false
        }

+ Response 200 (application/json)

        {
            "username":"user1",
            "hash":"user1hash",
            "email":"user1@email.com",
            "retired":false
        }

### Modify user information [PATCH]
This modifies the resource associated with username.
JSON with fields to be changed needs to be supplied, fields with no changes are not required.
The hash field cannot be changed (it is derived from application hash automatically).
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)

+ Request (application/merge-patch+json)

        {
            "email":"user1a@email.com"
        }

+ Response 200 (application/json)

        {
            "username":"user1",
            "hash":"user1hash",
            "email":"user1a@email.com",
            "retired":false
        }

### Delete a user [DELETE]
This deletes the resource associated with username.
Do not use unless intending to purge data! For retiring user account use PATCH instead to modify the field "retired".
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)

+ Response 204

## Subscription [/api/v0/{appname}/user/{username}/sub/{subid}]
### List subscription information [GET]
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)
   
+ Response 200 (application/json)

        {
            "subid":1,
            "start":1485611400000,
            "end":1488495600000,
            "level":"gold",
            "created":1485611400000,
            "modified":0
        }

### Create a new subscription [POST /api/v0/{appname}/user/{username}/sub]
NB: the URI for this endpoint doesn't contain subid.
Creates a new subscription resource, returns it.
Location header of the response also contains the URI of the created resource.
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
   
+ Request (application/json)

        {
            "planid":1,
            "start":1485558000000
        }

+ Response 201 (application/json)
    + Headers
    
            Location: /api/v0/{appname}/user/{username}/sub/123
    + Body

            {
                "subid":123,
                "start":1482793200000,
                "end":1488285200000,
                "level":"free",
                "created":1485611400000,
                "modified":0
            }

### Modify subscription information [PATCH]
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)
   
+ Request (application/merge-patch+json)

        {
            "level":"platinum",
            "tag":"loyalty2"
        }

+ Response 200 (application/json)

        {
            "subid":1,
            "start":1485611400000,
            "end":1488495600000,
            "level":"platinum",
            "tags":["loyalty2"],
            "created":1485611400000,
            "modified":1487011400000
        }

## Active subscription [/api/v0/{appname}/user/{username}/sub]
For convenience.
### List active subscription information [GET]
Lists the currently active subscription.
If more subscriptions are active, 409 Conflict is returned.
If no subscription is active, 404 is returned.
A subscription is active if start < today < end.
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
   
+ Response 200 (application/json)

        {
            "subid":1,
            "start":1485611400000,
            "end":1488495600000,
            "level":"gold",
            "created":1485611400000,
            "modified":0
        }

+ Response 409

### Modify active subscription information [PATCH]
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
   
+ Request (application/merge-patch+json)

        {
            "level":"platinum",
            "tag":"blackFriday16"
        }
        
+ Response 200 (application/json)

        {
            "subid":1,
            "start":1485611400000,
            "end":1488495600000,
            "level":"platinum",
            "tags":["blackFriday16"],
            "created":1485611400000,
            "modified":1486445500000
        }

## All subscriptions of specified user [/api/v0/{appname}/user/{username}/subs]
### List all subscriptions [GET]
This returns an array of all subscriptions.
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
   
+ Response 200 (application/json)

        [
            {
                "subid":1,
                "start":1485611400000,
                "end":1488495600000,
                "level":"platinum",
                "tags":["blackFriday16"],
                "created":1485611400000,
                "modified":1486445500000
            },
            {
                "subid":123,
                "start":1477954800000,
                "end":1485611400000,
                "level":"free",
                "created":1477954800000,
                "modified":0
            }
        ]

## Tags [/api/v0/{appname}/user/{username}/sub/{subid}/tags/{tag}]
Tags are strings associated with a subscription. They have to be alphanumeric.
If any tags are associated with a subscription, they are included in responses of that resource, as an array.
When creating or modifying a subscription, a single tag may be included in the "tag" field. It will be added to tags if it isnt there. For other operations involving tags, use the methods described below.
On subscription deletion, associated tags are deleted automatically.

### Add a tag to the subscription [PUT]
Adds a new tag to this subscription. (No-op if already exists.)
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)
    + tag: discount1

+ Response 201

+ Response 200

### Remove a tag from the subscription [DELETE]
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)
    + tag: discount1

+ Response 204

### List a single tag [GET]
For convenience, JSON with the tag is returned on success (the payload value is identical to the request URI parameter, so checking the return code should be enough in most cases).
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)
    + tag: discount1

+ Response 200 (application/json)

        {
            "tag":"discount1"
        }

### List tags of the subscription [GET /api/v0/{appname}/user/{username}/sub/{subid}/tags]
If there are no tags associated with the subscription, 404 is returned. Otherwise a JSON object containing an array of all tags is returned.
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)

+ Response 200 (application/json)

        {
            [
                "discount1",
                "event2",
                "VIP"
            ]
        }

## Timestamps [/api/v0/{appname}/user/{username}/sub/{subid}/ts]
A set of timestamps, anytime a subscription is used (any method), a new timestamp is added to the set.
### List timestamps [GET]
+ Parameters
    + appname: testApp (string, required)
    + username: user1 (string, required)
    + subid: 1 (number, required)

+ Response 200 (application/json)

        [
            {
                "time": 1488571892283,
                "contentID": 123
            },
            {
                "time": 1488571903516,
                "contentID": 124
            },
            {
                "time": 1488571913708,
                "contentID": 138
            }
        ]

## Discounts [/api/v0/{appname}/plan/{planname}/discount/{discountname}]
Discount resource can be used to create and configure discounts. Coupon codes can be generated from discounts, and given out.
Different plans can have discounts with same names.
### List discount information [GET]
+ Parameters
    + appname: testApp (string, required)
    + planname: gold (string, required)
    + discountname: 50off (string, required)

+ Response 200 (application/json)

        {
            planName: "gold",
            discountName: "50off",
            percentage: 50
        }

### Modify discount information [PATCH]
+ Parameters
    + appname: testApp (string, required)
    + planname: gold (string, required)
    + discountname: 50off (string, required)

+ Request 200 (application/merge-patch+json)

        {
            percentage: 100,
        }

+ Response 200 (application/json)

        {
            planName: "gold",
            discountName: "50off",
            percentage: 100
        }

### Create a new discount [PUT /api/v0/{appname}/plan/{planname}/discount/{discountname}]
NB: the URI for creation is different.
+ Parameters
    + appname: testApp (string, required)
    + planname: silver (string, required)
    + discountname: 50off (string, required)

+ Request 200 (application/merge-patch+json)

        {
            planName: "gold",
            discountName: "50off",
            percentage: 50
        }

+ Response 201 (application/json)

        {
            planName: "gold",
            discountName: "50off",
            percentage: 50
        }

### Delete a discount [DELETE]
+ Parameters
    + appname: testApp (string, required)
    + planname: silver (string, required)
    + discountname: 50off (string, required)

+ Response 204

### List all plan discounts [GET /api/v0/{appname}/plan/{planname}/discounts]
Returns an array of plan discounts.
+ Parameters
    + appname: testApp (string, required)
    + planname: silver (string, required)

+ Response 200 (application/json)

        [
            {
                planName: "silver",
                discountName: "30off",
                percentage: 30
            },
            {
                planName: "silver",
                discountName: "buy1get1",
                percentage: 100
            },
        ]

### List all app discounts [GET /api/v0/{appname}/discounts]
Returns an array of app discounts.
+ Parameters
    + appname: testApp (string, required)

+ Response 200 (application/json)

        [
            {
                planName: "gold",
                discountName: "50off",
                percentage: 50
            },
            {
                planName: "gold",
                discountName: "30off",
                percentage: 30
            },
            {
                planName: "silver",
                discountName: "30off",
                percentage: 30
            },
            {
                planName: "silver",
                discountName: "buy1get1",
                percentage: 100
            },
            {
                planName: "diamond",
                discountName: "founderGoodies",
                percentage: 100
            }
        ]

## Coupon codes [/api/v0/{appname}/code/{code}]
### Generate discount codes [POST /api/v0/{appname}/plan/{planname}/discount/{discountname}/codes{count}]
Generates a set of coupon codes for the specified discount. Request body is empty.
+ Parameters
    + appname: testApp (string, required)
    + planname: silver (string, required)
    + discountname: 30off (string, required)
    + count: 100 (number, required)

+ Response 201 (application/json)

        [
            "U5H9-HKDH-8RNX-1EX7",
            <!--100 codes here-->
            "7YQH-1FU7-E1HX-0BG9",
        ]

### Check coupon validity [GET]
For convenience, information about the code, discount and the original price is returned on success.
Failure indicated by 404 Not Found.
+ Parameters
    + appname: testApp (string, required)
    + code: U5H9-HKDH-8RNX-1EX7 (string, required)

+ Response 200 (application/json)
        
        {
            planName: "silver",
            discountName: "30off",
            percentage: 30
            originalPrice: 5
            code: "U5H9-HKDH-8RNX-1EX7",
        }

### Invalidate a coupon [DELETE]
+ Parameters
    + appname: testApp (string, required)
    + code: U5H9-HKDH-8RNX-1EX7 (string, required)

+ Response 204

# Group Datasets
Resources related to proccessing collected data, and providing datasets. Days and offset optional parameters can be used to specify the period of interest, where offset defines the number of days since the last day of interest. E.g. for a period of 2 weeks approximately a month ago, days would be 14, and offset would be 30, meaning the period of interest ends with (now - 30 days) and starts with (now - 30 days - 14 days).

## Users [/api/v0/{appname}/data/users]
Data about users.

### New users [GET /api/v0/{appname}/data/users/new{?days,offset}]
Returns an array of new user counts per day. Ordered from oldest to newest.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)
    + offset: 60 (number, optional)

+ Response 200 (application/json)

        [
            32,
            54,
            13,
            43,
            7
        ]

### Active users [GET /api/v0/{appname}/data/users/active{?days,offset,activity}]
Returns an array of active user counts per day during the specified period, active defined as number of uses per week, the threshold being the activity parameter, which defaults to 5.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)
    + activity: 5 (number, optional)

+ Response 200 (application/json)

        [
            345,
            365,
            387,
            409,
            389
        ]

### User attrition [GET /api/v0/{appname}/data/users/attrition{?days,offset,activity}]
An array of users lost per day. Only active users can be lost, new users can be lost only after they are active for at least one week. Activity treshold defaults to 5.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)

+ Response 200 (application/json)

        [
            20,
            4,
            7,
            8,
            11
        ]

### Portion of paying users [GET /api/v0/{appname}/data/users/paying{?days,offset,threshold}]
An array of days, with an integer representing the percentage of paying users. Threshhold can be used to specify the minimum amount of money spent per month to qualify as a paying user, it defauls to anything higher than 0.
+ Parameters
    + appname: testApp (string, required)
    + days: 10 (number, optional)
    + threshold: 5 (number, optional)

+ Response 200 (application/json)

        [
            3,
            3,
            5,
            6,
            7,
            8,
            9,
            12,
            12,
            10
        ]

## Subscriptions [/api/v0/{appname}/data/subs/]
Data about subscriptions
### Conversion rate [GET /api/v0/{appname}/data/subs/conv{?interval,days,offset,names}]
Provides information about percentage of users converting to other subscriptions. The interval parameter specifies the time window for a conversion, it defaults to 30 days. Optional flag names can be used to get percentages of plan names, rather than subscription levels.
+ Parameters
    + appname: testApp (string, required)
    + interval: 14 (number, optional)
    + names: false (boolean, optional)

+ Response 200 (application/json)

        {
            "free": {
                "none": 80,
                "bronze": 10,
                "gold": 8,
                "diamond": 2
            },
            "bronze": {
                "none": 76,
                "free": 20,
                "gold": 4
            },
            "gold": {
                "none": 50,
                "free": 5,
                "bronze": 30,
                "diamond": 15
            },
            "diamond": {
                "none": 60,
                "free": 25,
                "gold": 13,
                "bronze": 2
            }
        }


### Userbase composition by subscription type [GET /api/v0/{appname}/data/subs/comp{?days,offset}]
Array of objects containing the percentages of different subscriptions, of all users.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)

+ Response 200 (application/json)

        [
            {
                "free": 99,
                "bronze": 1
            },
            {
                "free": 95,
                "bronze": 4,
                "gold": 1
            },
            {
                "free": 92,
                "bronze": 5,
                "gold": 2,
                "diamond": 1
            },
            {
                "free": 91,
                "bronze": 6,
                "gold": 2,
                "diamond": 1
            },
            {
                "free": 90,
                "bronze": 6,
                "gold": 2,
                "diamond": 2
            }

        ]

### New subscriptions [GET /api/v0/{appname}/data/subs/new{?days,offset}]
Array of new subscription counts per day.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)

+ Response 200 (application/json)

        [
            15,
            24,
            8,
            28,
            3
        ]

## Plans [/api/v0/{appname}/data/plans]
### Adoption by plan type [GET /api/v0/{appname}/data/plans/comp{?days,offset}]
Array of objects containing the percentages of different subscriptions, of all users.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)

+ Response 200 (application/json)

        [
            {
                "free": 95,
                "founder": 4,
                "bronze": 1
            },
            {
                "free": 91,
                "founder": 4,
                "bronze": 4,
                "gold": 1
            },
            {
                "free": 89,
                "founder": 4,
                "bronze": 5,
                "lifetimeGold": 2
            },
            {
                "free": 88,
                "founder": 4,
                "bronze": 5,
                "lifetimeGold": 2,
                "diamond": 1
            },
            {
                "free": 87,
                "founder": 4,
                "bronze": 5,
                "gold": 1,
                "lifetimeGold": 2,
                "diamond": 1
            }

        ]
## Revenue [/api/v0/{appname}/data/rev]
### Revenue per user [GET /api/v0/{appname}/data/rev/user{?days,offset}]
Array of average monthly revenue per user, per day.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)

+ Response 200 (application/json)

        [
            0.05,
            0.06,
            0.05,
            0.06,
            0.07
        ]

### Revenue per paying user [GET /api/v0/{appname}/data/rev/paying{?days,offset,threshold}]
Array of average monthly revenue per paying user, per day. Threshold is used to specify how much the user should spend per month to be regarded as a paying user, the default value is 5.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)

+ Response 200 (application/json)

        [
            5.11,
            5.13,
            5.07,
            5.08,
            5.07
        ]

## Tags [/api/v0/{appname}/data/tags]
### Prevalence of specified tag over time [GET /api/v0/{appname}/data/tags/{tag}{?days,offset}]
Prevalence of subscriptions tagged with a tag specified by the tag parameter, as a percentage of all subscriptions.
+ Parameters
    + appname: testApp (string, required)
    + days: 5 (number, optional)
    + tag: "discount1"

+ Response 200 (application/json)

        [
            1,
            2,
            2,
            3,
            3
        ]

package main

import (
	"fmt"
	"strconv"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func selectRegistrationCode(code string) (string, error) {
	var c string
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM RegistrationCodes WHERE code = '%s'", code)).Scan(&c)
	if err != nil {
		return "", err
	}
	return c, nil
}

func deleteRegistrationCode(code string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM RegistrationCodes WHERE code = '%s'", code))
	if err != nil {
		return 0, err
	}
	af, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return af, nil
}

func insertRegistrationCodes(codes map[string]bool) (int64, error) {
	st, err := db.Prepare(fmt.Sprint("INSERT INTO RegistrationCodes VALUES (?)"))
	if err != nil {
		return 0, err
	}
	var affected int64
	for code := range codes {
		result, err := st.Exec(code)
		if err != nil {
			return 0, err
		}
		af, err := result.RowsAffected()
		if err != nil {
			return 0, err
		}
		affected += af
	}
	return affected, nil
}

func selectApp(appname string) (RESTApp, error) {
	var a RESTApp
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM Apps WHERE appname = '%s'", appname)).Scan(&a.AppName, &a.Secret, &a.Hash, &a.Created)
	if err != nil {
		return a, err
	}
	return a, nil
}

func insertApp(a RESTApp) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO Apps VALUES ('%s', '%s', '%s', '%d')", a.AppName, a.Secret, a.Hash, a.Created))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

//Partial update, appname is primary key and can't be changed, hash is calculated from secret.
//Supplying empty fields will make them retain their original values.
func updateApp(a RESTApp) (int64, error) {
	//parameters should be in the format of (table, field1, field1, ..., appname)
	result, err := db.Exec(fmt.Sprintf("UPDATE Apps SET secret = IF(CHAR_LENGTH('%s'),'%s',secret), hash = IF(CHAR_LENGTH('%s'),'%s',hash), created = IF(('%d'>0),'%d',created) WHERE appname = '%s'", a.Secret, a.Secret, a.Hash, a.Hash, a.Created, a.Created, a.AppName))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func deleteApp(appname string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM Apps WHERE appname = '%s'", appname))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func selectUser(table string, username string) (RESTUser, error) {
	var u RESTUser
	var b string
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE username = '%s'", table, username)).Scan(&u.Username, &u.Hash, &u.Email, &u.Created, &b)
	if err != nil {
		return u, err
	}
	if b == "1" {
		u.Retired = true
	}
	return u, nil
}

func insertUser(table string, u RESTUser) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO %s VALUES ('%s', '%s', '%s', '%d', %v)", table, u.Username, u.Hash, u.Email, u.Created, u.Retired))
	if err != nil {
		//log.Fatal(err)
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func selectAllUsers(appname string) ([]RESTUser, error) {
	var users []RESTUser
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `%s`", appname+"Users"))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var u RESTUser
		var b string
		err = rows.Scan(&u.Username, &u.Hash, &u.Email, &u.Created, &b)
		if err != nil {
			return nil, err
		}
		if b == "1" {
			u.Retired = true
		}
		users = append(users, u)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return users, nil
}

// func replaceUser(table string, u RESTUser) (int64, error) {
// 	result, err := db.Exec(fmt.Sprintf("UPDATE `%s` SET email = '%s', created = '%d', retired = %v WHERE username = '%s'", table, u.Email, u.Created, u.Retired, u.Username))
// 	if err != nil {
// 		return 0, err
// 	}
// 	affected, err := result.RowsAffected()
// 	if err != nil {
// 		return 0, err
// 	}
// 	return affected, nil
// }

func btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

//Partial update
//Supplying a struct with empty fields (zero value) as a parameter will result in the field retaining its current value.
func updateUser(table string, u RESTUser) (int64, error) {
	//prepare non-string fields for the query
	var retired int
	if u.Retired {
		retired = btoi(u.Retired)
	}
	//parameters should be in the format of (table, field1, field1, ..., username)
	result, err := db.Exec(fmt.Sprintf("UPDATE `%s` SET email = IF(CHAR_LENGTH('%s'),'%s',email), created = IF(('%d'>0),'%d',created), retired = IF(('%d'>0),'%d',retired) WHERE username = '%s'", table, u.Email, u.Email, u.Created, u.Created, retired, retired, u.Username))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func deleteUser(appname string, username string) (int64, error) {
	//first delete all information in other tables
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE username = '%s'", appname+"Timestamps", username))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	result, err = db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE username = '%s'", appname+"SubTags", username))
	if err != nil {
		return 0, err
	}
	af, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	result, err = db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE username = '%s'", appname+"Subs", username))
	if err != nil {
		return 0, err
	}
	af, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	result, err = db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE username = '%s'", appname+"Users", username))
	if err != nil {
		return 0, err
	}
	af, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	return affected, nil
}

func selectSubNoTags(table string, username string, subID string) (RESTSub, error) {
	var s RESTSub
	var b, throwaway string
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE username = '%s' AND subID = '%s'", table, username, subID)).Scan(&throwaway, &s.SubID, &s.Start, &s.End, &s.Level, &s.PlanName, &s.Price, &s.Created, &s.Modified, &b)
	if err != nil {
		return s, err
	}
	if b == "1" {
		s.Renew = true
	}
	return s, nil
}

func selectSub(appname string, username string, subID string) (RESTSub, error) {
	s, err := selectSubNoTags(appname+"Subs", username, subID)
	if err != nil {
		return s, err
	}
	tags, err := selectAllSubTags(appname+"SubTags", username, subID)
	if err != nil {
		return s, err
	}
	s.Tags = tags
	return s, nil
}

func selectAllSubs(appname string, username string) ([]RESTSub, error) {
	var subs []RESTSub
	var throwaway string
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `%s` WHERE username = '%s'", appname+"Subs", username))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var i int
	for rows.Next() {
		var s RESTSub
		var b string
		err := rows.Scan(&throwaway, &s.SubID, &s.Start, &s.End, &s.Level, &s.PlanName, &s.Price, &s.Created, &s.Modified, &b)
		if err != nil {
			return nil, err
		}
		if b == "1" {
			s.Renew = true
		}
		subs = append(subs, s)
		i++
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	for _, sub := range subs {
		tags, err := selectAllSubTags(appname+"SubTags", username, strconv.Itoa(sub.SubID))
		if err != nil {
			return nil, err
		}
		for _, t := range tags {
			sub.Tags = append(sub.Tags, t)
		}
	}
	return subs, nil
}

func insertSubNoTags(table string, username string, s RESTSub) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` VALUES ('%s', '%d', '%d', '%d', '%s', '%s', '%f', '%d', '%d', %v)", table, username, s.SubID, s.Start, s.End, s.Level, s.PlanName, s.Price, s.Created, s.Modified, s.Renew))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func insertSub(appname string, username string, s RESTSub) (int64, error) {
	af, err := insertSubNoTags(appname+"Subs", username, s)
	if err != nil {
		return 0, err
	}
	tags, err := selectAllSubTags(appname+"SubTags", username, strconv.Itoa(s.SubID))
	if err != nil {
		return 0, err
	}
	//insert tags specified by plan
TO_INSERT:
	for _, tag := range s.Tags {
		for _, t := range tags {
			if tag == t {
				//duplicate
				continue TO_INSERT
			}
		}
		af2, err := insertSubTag(appname+"SubTags", username, s.SubID, tag)
		if err != nil {
			return 0, err
		}
		af += af2
	}
	//insert tag specified by request
	if s.Tag != "" {
		//skip if s.Tag is a duplicate
		for _, t := range tags {
			if s.Tag == t {
				return af, nil
			}
		}
		for _, t := range s.Tags {
			if s.Tag == t {
				return af, nil
			}
		}
		af2, err := insertSubTag(appname+"SubTags", username, s.SubID, s.Tag)
		if err != nil {
			return 0, err
		}
		af += af2
	}
	return af, nil
}

//subID and username are primary keys and can't be changed
//if sub contains a tag, it is inserted unless already present
func updateSub(appname string, username string, s RESTSub) (int64, error) {
	//prepare non-string fields for the query
	var renew int
	if s.Renew {
		renew = btoi(s.Renew)
	}
	result, err := db.Exec(fmt.Sprintf("UPDATE `%s` SET start = IF(('%d'>0),'%d',start), end = IF(('%d'>0),'%d',end), level = IF(CHAR_LENGTH('%s'),'%s',level), name = IF(CHAR_LENGTH('%s'),'%s',name), price = IF(('%f'>0),'%f',price), created = IF(('%d'>0),'%d',created), modified = IF(('%d'>0),'%d',modified), renew = IF(('%d'>0),'%d',renew) WHERE username = '%s' AND subID = '%d'", appname+"Subs", s.Start, s.Start, s.End, s.End, s.Level, s.Level, s.PlanName, s.PlanName, s.Price, s.Price, s.Created, s.Created, s.Modified, s.Modified, renew, renew, username, s.SubID))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	if s.Tag != "" {
		has, err := hasSubTag(appname+"SubTags", username, strconv.Itoa(s.SubID), s.Tag)
		if err != nil {
			return 0, err
		}
		if has {
			return affected, nil
		}
		affected2, err := insertSubTag(appname+"SubTags", username, s.SubID, s.Tag)
		if err != nil {
			return 0, err
		}
		affected += affected2
	}
	return affected, nil
}

func selectAllSubTags(table string, username string, subID string) ([]string, error) {
	var tags []string
	rows, err := db.Query(fmt.Sprintf("SELECT tag FROM `%s` WHERE username = '%s' AND subID = '%s'", table, username, subID))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var s string
		err := rows.Scan(&s)
		if err != nil {
			return nil, err
		}
		tags = append(tags, s)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func selectAllPlanTags(table string, planName string) ([]string, error) {
	var tags []string
	rows, err := db.Query(fmt.Sprintf("SELECT tag FROM `%s` WHERE planName = '%s'", table, planName))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var s string
		err := rows.Scan(&s)
		if err != nil {
			return nil, err
		}
		tags = append(tags, s)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return tags, nil
}

func hasSubTag(table string, username string, subID string, tag string) (bool, error) {
	var throwaway string
	err := db.QueryRow(fmt.Sprintf("SELECT tag FROM `%s` WHERE username = '%s' AND subID = '%s' AND tag = '%s'", table, username, subID, tag)).Scan(&throwaway)
	if err != nil {
		if err != sql.ErrNoRows {
			return false, err
		}
	}
	if err == sql.ErrNoRows {
		return false, nil
	}
	return true, nil
}

func insertSubTag(table string, username string, subID int, tag string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` (username, subID, tag) VALUES ('%s', '%d', '%s')", table, username, subID, tag))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func insertPlanTags(table string, p RESTPlan) (int64, error) {
	if p.Tags == nil {
		return 0, nil
	}
	var affected int64
	//select tags, check whether each tag is present before adding it
	tags, err := selectAllPlanTags(table, p.Name)
	if err != nil {
		return 0, err
	}
TO_INSERT:
	for _, tag := range p.Tags {
		for _, t := range tags {
			if tag == t {
				//skip inserting tags that are already present
				continue TO_INSERT
			}
		}
		result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` (planName, tag) VALUES ('%s', '%s')", table, p.Name, tag))
		if err != nil {
			return 0, err
		}
		af, err := result.RowsAffected()
		if err != nil {
			return 0, err
		}
		affected += af
	}
	return affected, nil
}

func deleteSubTag(table string, username string, subID string, tag string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE username = '%s' AND subID = '%s' AND tag = '%s'", table, username, subID, tag))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func deleteAllSubTags(table string, username string, subID string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE username = '%s' AND subID = '%s'", table, username, subID))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func deletePlanTag(table string, planName string, tag string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE planName = '%s' AND tag = '%s'", table, planName, tag))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func deleteAllPlanTags(table string, planName string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE planName = '%s'", table, planName))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func selectTimestamps(table string, username string, subID string) ([]RESTTimestamp, error) {
	var ts []RESTTimestamp
	rows, err := db.Query(fmt.Sprintf("SELECT time, contentID FROM `%s` WHERE username = '%s' AND subID = '%s'", table, username, subID))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var t RESTTimestamp
		err := rows.Scan(&t.Time, &t.ContentID)
		if err != nil {
			return nil, err
		}
		ts = append(ts, t)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return ts, nil
}

//index doesn't need to be supplied since it's an AUTO INCREMENT column
func insertTimestamp(table string, username string, subID int, t RESTTimestamp) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` (username, subID, time, contentID) VALUES ('%s', '%d', '%d', '%d')", table, username, subID, t.Time, t.ContentID))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

//number of timestamps can serve as a 'counter' of usage
func countTimestamps(table string, username string, subID int) (int64, error) {
	var result int64
	err := db.QueryRow(fmt.Sprintf("SELECT COUNT (*) FROM `%s` WHERE username = '%s' AND subID = '%d'", table, username, subID)).Scan(&result)
	if err != nil {
		return 0, err
	}
	return result, nil
}

func selectPlanNoTags(table string, planName string) (RESTPlan, error) {
	var p RESTPlan
	var b1, b2 string
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE name = '%s'", table, planName)).Scan(&p.Name, &p.Level, &p.Price, &p.Length, &b1, &b2)
	if err != nil {
		return p, err
	}
	if b1 == "1" {
		p.Renew = true
	}
	if b2 == "1" {
		p.Active = true
	}
	return p, nil
}

func selectPlan(appname string, planName string) (RESTPlan, error) {
	p, err := selectPlanNoTags(appname+"Plans", planName)
	if err != nil {
		return p, err
	}
	tags, err := selectAllPlanTags(appname+"PlanTags", planName)
	if err != nil {
		return p, err
	}
	p.Tags = tags
	return p, nil
}

func insertActivePlan(appname string, p RESTPlan) (int64, error) {
	p.Name = "active"
	affected, err := insertPlanNoTags(appname+"ActivePlan", p)
	if err != nil {
		return 0, err
	}
	affected2, err := insertPlanTags(appname+"ActivePlanTags", p)
	if err != nil {
		return 0, err
	}
	return affected + affected2, nil
}

func selectActivePlan(appname string) (RESTPlan, error) {
	p, err := selectActivePlanNoTags(appname + "ActivePlan")
	if err != nil {
		return p, err
	}
	//we can be sure this is only called when there is exactly one active plan
	tags, err := selectAllPlanTags(appname+"ActivePlanTags", p.Name)
	if err != nil {
		return p, err
	}
	p.Tags = tags
	return p, nil
}

//returns errNoPlans if there is no active plan
func selectActivePlanNoTags(table string) (RESTPlan, error) {
	var p RESTPlan
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `%s`", table))
	if err != nil {
		if err != sql.ErrNoRows {
			return p, err
		}
		return p, errNoPlans
	}
	defer rows.Close()
	for rows.Next() {
		if !p.isEmpty() {
			//should never happen, as on setting new active plan the old one is deleted
			return p, errMorePlans
		}
		var b1, b2 string
		rows.Scan(&p.Name, &p.Level, &p.Price, &p.Length, &b1, &b2)
		if err != nil {
			return p, err
		}
		if b1 == "1" {
			p.Renew = true
		}
		if b2 == "1" {
			p.Active = true
		}
	}
	err = rows.Err()
	if err != nil {
		return p, err
	}
	return p, nil
}

func deleteActivePlan(appname string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("TRUNCATE TABLE `%s`", appname+"ActivePlanTags"))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	result, err = db.Exec(fmt.Sprintf("TRUNCATE TABLE `%s`", appname+"ActivePlan"))
	if err != nil {
		return 0, err
	}
	af, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	return affected, nil
}

func selectAllPlans(appname string) ([]RESTPlan, error) {
	plans, err := selectAllPlansNoTags(appname + "Plans")
	if err != nil {
		return nil, err
	}
	for i, p := range plans {
		tags, err := selectAllPlanTags(appname+"PlanTags", p.Name)
		if err != nil {
			return nil, err
		}
		p.Tags = tags
		plans[i] = p
	}
	return plans, nil
}

//select all plans except the duplicate in active slot, active slot is has active field set to true
func selectAllPlansNoTags(table string) ([]RESTPlan, error) {
	var plans []RESTPlan
	//don't select plan with id 0, that's the active plan (duplicated from one of the other plans)
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `%s` WHERE active = false", table))
	if err != nil {
		return plans, err
	}
	defer rows.Close()
	for rows.Next() {
		var p RESTPlan
		var b1, b2 string
		err := rows.Scan(&p.Name, &p.Level, &p.Price, &p.Length, &b1, &b2)
		if err != nil {
			return nil, err
		}
		if b1 == "1" {
			p.Renew = true
		}
		if b2 == "1" {
			p.Active = true
		}
		plans = append(plans, p)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return plans, nil
}

func insertPlanNoTags(table string, p RESTPlan) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` VALUES ('%s', '%s', '%f', '%d', %v, %v)", table, p.Name, p.Level, p.Price, p.Length, p.Renew, p.Active))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func insertPlan(appname string, p RESTPlan) (int64, error) {
	affected, err := insertPlanNoTags(appname+"Plans", p)
	if err != nil {
		return 0, err
	}
	affected2, err := insertPlanTags(appname+"PlanTags", p)
	if err != nil {
		return 0, err
	}
	return affected + affected2, nil
}

func updatePlan(appname string, p RESTPlan) (int64, error) {
	//prepare non-string fields for the query
	var renew, active int
	if p.Renew {
		renew = btoi(p.Renew)
	}
	if p.Active {
		active = btoi(p.Active)
	}
	//parameters should be in the format of (table, field1, field1, ..., login)
	result, err := db.Exec(fmt.Sprintf("UPDATE `%s` SET level = IF(CHAR_LENGTH('%s'),'%s',level), price = IF(('%f'>0),'%f',price), length = IF(('%d'>0),'%d',length), renew = IF(('%d'>0),'%d',renew), active = IF(('%d'>0),'%d',active) WHERE name = '%s'", appname+"Plans", p.Level, p.Level, p.Price, p.Price, p.Length, p.Length, renew, renew, active, active, p.Name))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	if p.Tags != nil {
		affected2, err := insertPlanTags(appname+"PlanTags", p)
		if err != nil {
			return 0, err
		}
		affected += affected2
	}
	return affected, nil
}

func deletePlan(appname string, planName string) (int64, error) {
	affected, err := deleteAllPlanTags(appname+"PlanTags", planName)
	if err != nil {
		return 0, err
	}
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE planName = '%s'", appname+"Codes", planName))
	if err != nil {
		return 0, err
	}
	af, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	result, err = db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE planName = '%s'", appname+"Discounts", planName))
	if err != nil {
		return 0, err
	}
	af, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	result, err = db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE name = '%s'", appname+"Plans", planName))
	if err != nil {
		return 0, err
	}
	af, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}
	affected += af
	return affected, nil
}

func insertAdmin(table string, admin RESTAdmin) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` VALUES ('%s', '%s', '%s')", table, admin.Login, admin.Password, admin.Email))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func selectAdmin(table string, login string) (RESTAdmin, error) {
	var admin RESTAdmin
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE login = '%s'", table, login)).Scan(&admin.Login, &admin.Password, &admin.Email)
	if err != nil {
		return admin, err
	}
	return admin, nil
}

func updateAdmin(table string, admin RESTAdmin) (int64, error) {
	//parameters should be in the format of (table, field1, field1, ..., login)
	result, err := db.Exec(fmt.Sprintf("UPDATE `%s` SET password = IF(CHAR_LENGTH('%s'),'%s',password), email = IF(CHAR_LENGTH('%s'),'%s',email) WHERE login = '%s'", table, admin.Password, admin.Password, admin.Email, admin.Email, admin.Login))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func deleteAdmin(table string, login string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE login = '%s'", table, login))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func selectDiscount(table string, planName string, discountName string) (RESTDiscount, error) {
	var d RESTDiscount
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE planName = '%s' AND discountName = '%s'", table, planName, discountName)).Scan(&d.PlanName, &d.DiscountName, &d.Percentage)
	if err != nil {
		return d, err
	}
	return d, nil
}

func insertDiscount(table string, d RESTDiscount) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("INSERT INTO `%s` VALUES ('%s', '%s', '%f')", table, d.PlanName, d.DiscountName, d.Percentage))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func updateDiscount(table string, d RESTDiscount) (int64, error) {
	//parameters should be in the format of (table, field1, field1, ..., login)
	result, err := db.Exec(fmt.Sprintf("UPDATE `%s` SET percentage = IF(('%f'>0),'%f',percentage) WHERE planName = '%s' AND discountName = '%s'", table, d.Percentage, d.Percentage, d.PlanName, d.DiscountName))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	fmt.Println(affected)
	return affected, nil
}

func deleteDiscount(table string, planName string, discountName string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE planName = '%s' AND discountName = '%s'", table, planName, discountName))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

func selectPlanDiscounts(table string, planName string) ([]RESTDiscount, error) {
	var discounts []RESTDiscount
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `%s` WHERE planName = '%s'", table, planName))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var d RESTDiscount
		err := rows.Scan(&d.PlanName, &d.DiscountName, &d.Percentage)
		if err != nil {
			return nil, err
		}
		discounts = append(discounts, d)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return discounts, nil
}

func selectAppDiscounts(table string) ([]RESTDiscount, error) {
	var discounts []RESTDiscount
	rows, err := db.Query(fmt.Sprintf("SELECT * FROM `%s`", table))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var d RESTDiscount
		err := rows.Scan(&d.PlanName, &d.DiscountName, &d.Percentage)
		if err != nil {
			return nil, err
		}
		discounts = append(discounts, d)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return discounts, nil
}

func existsDiscountCode(appname string, code string) (bool, error) {
	var exists string
	err := db.QueryRow(fmt.Sprintf("SELECT 1 FROM `%s` WHERE code = '%s'", appname+"Codes", code)).Scan(&exists)
	if err != nil {
		if err != sql.ErrNoRows {
			return false, err
		}
		return false, nil
	}
	return true, nil
}

func selectDiscountCode(appname string, code string) (RESTDiscountCode, error) {
	var c RESTDiscountCode
	err := db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE code = '%s'", appname+"Codes", code)).Scan(&c.Code, &c.PlanName, &c.DiscountName)
	if err != nil {
		return c, err
	}
	err = db.QueryRow(fmt.Sprintf("SELECT * FROM `%s` WHERE planName = '%s' AND discountName = '%s'", appname+"Discounts", c.PlanName, c.DiscountName)).Scan(&c.PlanName, &c.DiscountName, &c.Percentage)
	if err != nil {
		return c, err
	}
	return c, nil
}

func insertDiscountCodes(appname string, codes map[string]RESTDiscountCode) (int64, error) {
	st, err := db.Prepare(fmt.Sprintf("INSERT INTO `%s` (code, planName, discountName) VALUES (?, ?, ?)", appname+"Codes"))
	if err != nil {
		return 0, err
	}
	var affected int64
	for _, code := range codes {
		result, err := st.Exec(code.Code, code.PlanName, code.DiscountName)
		if err != nil {
			return affected, err
		}
		af, err := result.RowsAffected()
		if err != nil {
			return affected, err
		}
		affected += af
	}
	return affected, nil
}

func deleteDiscountCode(appname string, code string) (int64, error) {
	result, err := db.Exec(fmt.Sprintf("DELETE FROM `%s` WHERE code = '%s'", appname+"Codes", code))
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}

//createApps function to create the Apps table,
//on server startup, check whether Apps exists, if not call createApps
func createApps() error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE Apps\n(\nappname varchar(64),\nsecret varchar(64),\nhash varchar(64),\ncreated bigint,\nPRIMARY KEY (appname)\n);"))
	return err
}

func createRegistrationCodes() error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE RegistrationCodes\n(\ncode varchar(64),\nPRIMARY KEY (code)\n);"))
	return err
}

func createAppAdmins(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nlogin varchar(64),\npassword varchar(64),\nemail varchar(64),\nPRIMARY KEY (login)\n);", appname+"Admins"))
	if err != nil {
		return err
	}
	return nil
}

func createAppUsers(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nusername varchar(64),\nhash char(64),\nemail varchar(64),\ncreated bigint,\nretired bool,\nPRIMARY KEY (username)\n);", appname+"Users"))
	if err != nil {
		return err
	}
	return nil
}

func createAppPlans(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nname varchar(64),\nlevel varchar(64),\nprice double,\nlength int,\nrenew bool,\nactive bool,\nPRIMARY KEY (name)\n);", appname+"Plans"))
	if err != nil {
		return err
	}
	return nil
}

func createAppActivePlan(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nname varchar(64),\nlevel varchar(64),\nprice double,\nlength int,\nrenew bool,\nactive bool,\nPRIMARY KEY (name)\n);", appname+"ActivePlan"))
	if err != nil {
		return err
	}
	return nil
}

func createAppSubs(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nusername varchar(64),\nsubID int,\nstart bigint,\nend bigint,\nlevel varchar(64),\nname varchar(64),\nprice double,\ncreated bigint,\nmodified bigint,\nrenew bool,\nPRIMARY KEY (username, subID),\nFOREIGN KEY (username) REFERENCES %s(username)\n);", appname+"Subs", appname+"Users"))
	if err != nil {
		return err
	}
	return nil
}

func createAppTimestamps(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\ni SERIAL,\nusername varchar(64),\nsubID int,\ntime bigint,\ncontentID int,\nPRIMARY KEY (i),\nFOREIGN KEY (username, subID) REFERENCES %s(username, subID)\n);", appname+"Timestamps", appname+"Subs"))
	return err
}

func createAppPlanTags(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nplanName varchar(64),\ntag varchar(32),\nFOREIGN KEY (planName) REFERENCES %s(name),\nPRIMARY KEY (planName, tag)\n);", appname+"PlanTags", appname+"Plans"))
	return err
}

func createAppActivePlanTags(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nplanName varchar(64),\ntag varchar(32),\nFOREIGN KEY (planName) REFERENCES %s(name),\nPRIMARY KEY (planName, tag)\n);", appname+"ActivePlanTags", appname+"ActivePlan"))
	return err
}

func createAppSubTags(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nusername varchar(64),\nsubID int,\ntag varchar(32),\nFOREIGN KEY (username, subID) REFERENCES %s(username, subID),\nPRIMARY KEY (username, subID, tag)\n);", appname+"SubTags", appname+"Subs"))
	return err
}

func createAppDiscounts(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\nplanName varchar(64),\ndiscountName varchar(64),\npercentage double,\nFOREIGN KEY (planName) REFERENCES %s(name),\nPRIMARY KEY (planName, discountName)\n);", appname+"Discounts", appname+"Plans"))
	return err
}

func createAppCodes(appname string) error {
	_, err := db.Exec(fmt.Sprintf("CREATE TABLE %s\n(\ncode varchar(64),\nplanName varchar(64),\ndiscountName varchar(64),\nFOREIGN KEY (planName, discountName) REFERENCES %s(planName, discountName),\nPRIMARY KEY (code)\n);", appname+"Codes", appname+"Discounts"))
	return err
}

func dropAppCodes(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS `%s` CASCADE", appname+"Codes"))
	return err
}

func dropAppDiscounts(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS `%s` CASCADE", appname+"Discounts"))
	return err
}

func dropAppSubTags(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS `%s` CASCADE", appname+"SubTags"))
	return err
}

func dropAppPlanTags(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"PlanTags"))
	return err
}

func dropAppActivePlanTags(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"ActivePlanTags"))
	return err
}

func dropAppSubs(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"Subs"))
	if err != nil {
		return err
	}
	return nil
}

func dropAppPlans(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"Plans"))
	if err != nil {
		return err
	}
	return nil
}

func dropAppActivePlan(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"ActivePlan"))
	if err != nil {
		return err
	}
	return nil
}

func dropAppUsers(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"Users"))
	if err != nil {
		return err
	}
	return nil
}

func dropAppAdmins(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"Admins"))
	if err != nil {
		return err
	}
	return nil
}

func dropAppTimestamps(appname string) error {
	_, err := db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %s CASCADE", appname+"Timestamps"))
	return err
}

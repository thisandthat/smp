package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/captaincodeman/couponcode"
	_ "github.com/go-sql-driver/mysql"
)

var cleanup = flag.Bool("cleanup", false, "drop tables after running tests")

var names = []string{"abel", "ben", "clive", "declan", "emilio", "frederick", "gerard", "helmut", "ivan", "john"}

var planNames = []string{"free", "free", "bronze", "silver", "gold", "longSilver", "longGold", "lifetimeGold", "freeGold"}

var planLevels = []string{"free", "bronze", "silver", "gold"}

func prepareUsers(appHash string, count int) []*RESTUser {
	hasher := hmac.New(sha256.New, []byte(appHash))
	var users []*RESTUser
	for i := 0; i < count; i++ {
		r := rand.Int()
		var u RESTUser
		u.Username = names[r%len(names)] + strconv.Itoa(r%100) + strconv.Itoa(i)
		if r%2 == 1 {
			u.Email = u.Username + "@email.com"
		}
		hasher.Write([]byte(u.Username))
		u.Hash = fmt.Sprintf("%x", hasher.Sum(nil)) //user specific secret
		u.Created = time.Now().Unix()
		users = append(users, &u)
	}
	return users
}

func (u *RESTUser) getCreated() int64 {
	return u.Created
}

func (u *RESTUser) setCreated(timestamp int64) {
	u.Created = timestamp
}

type creation interface {
	getCreated() int64
	setCreated(timestamp int64)
}

func adjustCreatedMod(modulo int, elems []creation) {
	for i := 0; i < len(elems); i++ {
		t := time.Unix(elems[i].getCreated(), 0).AddDate(0, 0, -i%modulo).Add(time.Hour * -1).Unix()
		elems[i].setCreated(t)
	}
}

func adjustCreatedModNBack(modulo, offset int, elems []creation) {
	for i := 0; i < len(elems); i++ {
		//-1 day by default because we don't want to simulate data for the current day
		//since we check at most the previous day
		t := time.Unix(elems[i].getCreated(), 0).AddDate(0, 0, -offset-i%modulo).Add(time.Hour * -1).Unix()
		elems[i].setCreated(t)
	}
}

func checkApps(table string) error {
	//check whether table exists
	var name string
	err := db.QueryRow(fmt.Sprintf("SELECT (table_name) FROM information_schema.tables WHERE table_schema = 'xpospe03' AND table_name = '%s' LIMIT 1", table)).Scan(&name)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Printf("Table %s doesn't exist.\n", table)
			fmt.Printf("Trying to create table %s.\n", table)
			err = createApps()
			if err != nil {
				return err
			}
		} else {
			fmt.Printf("Error when checking table %s: %s\n", table, err)
			return err
		}
	}
	return err
}

func checkRegistrationCodes(table string) error {
	//check whether table exists
	var name string
	err := db.QueryRow(fmt.Sprintf("SELECT (table_name) FROM information_schema.tables WHERE table_schema = 'xpospe03' AND table_name = '%s' LIMIT 1", table)).Scan(&name)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Printf("Table %s doesn't exist.\n", table)
			fmt.Printf("Trying to create table %s.\n", table)
			err = createRegistrationCodes()
			if err != nil {
				return err
			}
			//insert a few codes
			cc := couponcode.New(4, 4)
			codes := make(map[string]bool)
			for len(codes) < 10 {
				code := cc.Generate()
				codes[code] = true
			}
			_, err := insertRegistrationCodes(codes)
			if err != nil {
				return err
			}
		} else {
			fmt.Printf("Error when checking table %s: %s\n", table, err)
			return err
		}
	}
	return err
}

func populateApp(appname, secret string) (RESTApp, error) {
	//calculate hash
	salt := make([]byte, 32)
	_, err := rand.Read(salt)
	fmt.Println("salt is:", salt)
	hasher := hmac.New(sha256.New, salt)
	a := RESTApp{}
	a.AppName = appname
	a.Secret = secret
	hasher.Write([]byte(a.Secret))
	a.Hash = fmt.Sprintf("%x", hasher.Sum(nil))
	a.Created = time.Now().Unix()
	_, err = insertApp(a)
	if err != nil {
		return a, err
	}
	err = initApp(a.AppName, "admin")
	if err != nil {
		return a, err
	}
	return a, err
}

func preparePlans() []RESTPlan {
	var plans []RESTPlan
	for i, name := range planNames {
		p := RESTPlan{}
		p.Name = name
		p.Length = 30 + 30*(i/4)
		p.Active = false
		p.Price = float64(i * 5)
		if name == "free" {
			p.Active = true
		}
		if name == "lifetimeGold" {
			p.Length = 0
		}
		for _, level := range planLevels {
			if strings.Contains(strings.ToLower(name), level) {
				p.Level = level
			}

		}
		if name == "freeGold" {
			p.Price = 0
		}
		plans = append(plans, p)
	}
	return plans
}

func populatePlans(appname string) ([]RESTPlan, error) {
	plans := preparePlans()
	for _, p := range plans {
		if p.Name == "free" {
			continue
		}
		af, err := insertPlan(appname, p)
		if err != nil {
			return plans, err
		}
		fmt.Println("Inserting plan, rows affected:", af)
	}
	err := populateDiscounts(appname, plans)
	if err != nil {
		return nil, err
	}
	return plans, nil
}

func populateSubs(appname string, users []*RESTUser, plans []RESTPlan) error {
	//free sub for everyone
	for _, u := range users {
		fmt.Println(*u)
	}
	for i, u := range users {
		var s RESTSub
		s.SubID = 0
		s.Price = plans[0].Price
		s.PlanName = plans[0].Name
		s.Level = plans[0].Level
		s.Renew = plans[0].Renew
		s.Created = time.Unix(u.Created, 0).Add(time.Minute).Unix() //simulating that account setup takes a minute
		s.Modified = s.Created
		s.Start = s.Created
		s.End = time.Unix(s.Created, 0).AddDate(0, 0, plans[0].Length).Unix()
		_, err := insertSub(appname, u.Username, s)
		if err != nil {
			return err
		}
		fmt.Println("Sub added to user", i)
	}
	//paid sub for some
	for i, u := range users {
		if i%2 == 0 {
			continue
		}
		pid := i % len(plans)
		var s RESTSub
		s.SubID = 1
		s.Price = plans[pid].Price
		s.PlanName = plans[pid].Name
		s.Level = plans[pid].Level
		s.Renew = plans[pid].Renew
		s.Created = time.Unix(u.Created, 0).Add(time.Minute * 2).Unix() //excited about product after a minute, so they buy a paid sub
		s.Modified = s.Created
		s.Start = s.Created
		s.End = time.Unix(s.Created, 0).AddDate(0, 0, plans[pid].Length).Unix()
		if i%5 == 0 {
			s.Tag = "blackFriday16"
		}
		_, err := insertSub(appname, u.Username, s)
		if err != nil {
			return err
		}
		fmt.Println("Sub added to user", i)
	}
	return nil
}

func populateTimestamps(appname string, users []*RESTUser) error {
	for i, u := range users {
		for j := 0; j < 10; j++ {
			var t RESTTimestamp
			t.Time = time.Unix(u.Created, 0).Add(time.Minute*time.Duration(5*j)).AddDate(0, 0, j).Unix()
			if j%2 == 0 {
				t.ContentID = j % 3
			}
			subID := i % 2
			//at least one timestamp for the free sub (with subID 0)
			if j == 0 {
				subID = 0
			}
			_, err := insertTimestamp(appname+"Timestamps", u.Username, subID, t)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func populateDiscounts(appname string, plans []RESTPlan) error {
	for i, p := range plans {
		if p.Name == "free" {
			continue
		}
		percentage := ((i % 4) + 1) * 20
		var d RESTDiscount
		d.DiscountName = p.Name + strconv.Itoa(percentage) + "off"
		d.Percentage = float64(percentage) / 100
		d.PlanName = p.Name
		_, err := insertDiscount(appname+"Discounts", d)
		if err != nil {
			return err
		}
	}
	return nil
}

func populateDB(userCount int) {
	//check whether Apps table exists, create if it doesn't
	err := checkApps("Apps")
	if err != nil {
		log.Fatal(err)
	}
	err = checkRegistrationCodes("RegistrationCodes")
	if err != nil {
		log.Fatal(err)
	}
	//firstTestApp
	fmt.Println("Inserting app.")
	a, err := populateApp("firstTestApp", "verysecret")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Inserting plans.")
	plans, err := populatePlans(a.AppName)
	if err != nil {
		log.Fatal(err)
	}
	users := prepareUsers(a.Hash, userCount)
	creations := make([]creation, len(users))
	for i, u := range users {
		creations[i] = creation(u)
	}
	adjustCreatedModNBack(10, 14, creations)
	for i, c := range creations {
		users[i] = c.(*RESTUser)
	}
	fmt.Println("Inserting users.")
	for _, u := range users {
		insertUser(a.AppName+"Users", *u)
		if err != nil {
			log.Fatal(err)
		}
	}
	fmt.Println("Inserting subs.")
	err = populateSubs(a.AppName, users, plans)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Inserting timestamps.")
	err = populateTimestamps(a.AppName, users)
	if err != nil {
		log.Fatal(err)
	}
}

func TestMain(m *testing.M) {
	flag.Parse()
	fmt.Println("Test started.")
	fmt.Printf("time:\t%v\t%d\n", time.Now(), time.Now().Unix())
	var err error
	db, err = sql.Open("mysql", dbCredentials)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		// do something here
		fmt.Println("Can't ping db.")
		log.Fatal(err)
	}
	userCount := 50
	//populate db with test data here
	populateDB(userCount)

	m.Run()

	if *cleanup {
		dropAll("firstTestApp")
		_, err = deleteApp("firstTestApp")
		if err != nil {
			log.Fatal(err)
		}
	}
}

func Test_countNewUsers(t *testing.T) {
	type args struct {
		appname string
		days    int
		offset  int
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{"NewUsers1day", args{"firstTestApp", 1, 0}, []int{0}, false},
		{"NewUsers2days", args{"firstTestApp", 2, 0}, []int{0, 0}, false},
		{"NewUsers3days", args{"firstTestApp", 3, 0}, []int{0, 0, 0}, false},
		{"NewUsers4days", args{"firstTestApp", 4, 0}, []int{0, 0, 0, 0}, false},
		{"NewUsers5days", args{"firstTestApp", 5, 0}, []int{0, 0, 0, 0, 0}, false},
		{"NewUsers6days", args{"firstTestApp", 6, 0}, []int{0, 0, 0, 0, 0, 0}, false},
		{"NewUsers7days", args{"firstTestApp", 7, 0}, []int{0, 0, 0, 0, 0, 0, 0}, false},
		{"NewUsers7days1back", args{"firstTestApp", 7, 1}, []int{0, 0, 0, 0, 0, 0, 0}, false},
		{"NewUsers5days10back", args{"firstTestApp", 5, 10}, []int{5, 0, 0, 0, 0}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countNewUsers(tt.args.appname, tt.args.days, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("countNewUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countNewUsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countActiveUsers(t *testing.T) {
	type args struct {
		appname  string
		days     int
		offset   int
		activity int
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{"PayingUsers1day", args{"firstTestApp", 1, 0, 5}, []int{0}, false},
		{"PayingUsers2days", args{"firstTestApp", 2, 0, 5}, []int{0, 0}, false},
		{"PayingUsers3days", args{"firstTestApp", 3, 0, 5}, []int{0, 0, 0}, false},
		{"PayingUsers4days", args{"firstTestApp", 4, 0, 5}, []int{5, 0, 0, 0}, false},
		{"PayingUsers5days", args{"firstTestApp", 5, 0, 5}, []int{10, 5, 0, 0, 0}, false},
		{"PayingUsers6days", args{"firstTestApp", 6, 0, 5}, []int{15, 10, 5, 0, 0, 0}, false},
		{"PayingUsers7days", args{"firstTestApp", 7, 0, 5}, []int{20, 15, 10, 5, 0, 0, 0}, false},
		{"PayingUsers8days", args{"firstTestApp", 8, 0, 5}, []int{25, 20, 15, 10, 5, 0, 0, 0}, false},
		{"PayingUsers9days", args{"firstTestApp", 9, 0, 5}, []int{30, 25, 20, 15, 10, 5, 0, 0, 0}, false},
		{"PayingUsers7days1back", args{"firstTestApp", 7, 1, 5}, []int{25, 20, 15, 10, 5, 0, 0}, false},
		{"PayingUsers7days4back", args{"firstTestApp", 7, 4, 5}, []int{40, 35, 30, 25, 20, 15, 10}, false},
		{"PayingUsers7days7back", args{"firstTestApp", 7, 7, 5}, []int{35, 40, 40, 40, 35, 30, 25}, false},
		{"PayingUsers5days10back", args{"firstTestApp", 5, 10, 5}, []int{30, 35, 40, 40, 40}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countActiveUsers(tt.args.appname, tt.args.days, tt.args.offset, tt.args.activity)
			if (err != nil) != tt.wantErr {
				t.Errorf("countActiveUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countActiveUsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countLostUsers(t *testing.T) {
	type args struct {
		appname  string
		days     int
		offset   int
		activity int
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{"LostUsers1day", args{"firstTestApp", 1, 0, 5}, []int{0}, false},
		{"LostUsers2days", args{"firstTestApp", 2, 0, 5}, []int{0, 0}, false},
		{"LostUsers3days", args{"firstTestApp", 3, 0, 5}, []int{-5, 0, 0}, false},
		{"LostUsers4days", args{"firstTestApp", 4, 0, 5}, []int{-5, -5, 0, 0}, false},
		{"LostUsers5days", args{"firstTestApp", 5, 0, 5}, []int{-5, -5, -5, 0, 0}, false},
		{"LostUsers6days", args{"firstTestApp", 6, 0, 5}, []int{-5, -5, -5, -5, 0, 0}, false},
		{"LostUsers7days", args{"firstTestApp", 7, 0, 5}, []int{-5, -5, -5, -5, -5, 0, 0}, false},
		{"LostUsers8days", args{"firstTestApp", 8, 0, 5}, []int{0, -5, -5, -5, -5, -5, 0, 0}, false},
		{"LostUsers9days", args{"firstTestApp", 9, 0, 5}, []int{0, 0, -5, -5, -5, -5, -5, 0, 0}, false},
		{"LostUsers28days", args{"firstTestApp", 28, 0, 5}, []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 0, 0, 0, 0, 0, -5, -5, -5, -5, -5, 0, 0}, false},
		{"LostUsers7days1back", args{"firstTestApp", 7, 1, 5}, []int{0, -5, -5, -5, -5, -5, 0}, false},
		{"LostUsers7days4back", args{"firstTestApp", 7, 4, 5}, []int{0, 0, 0, 0, -5, -5, -5}, false},
		{"LostUsers7days7back", args{"firstTestApp", 7, 7, 5}, []int{5, 5, 0, 0, 0, 0, 0}, false},
		{"LostUsers7days14back", args{"firstTestApp", 7, 14, 5}, []int{0, 0, 0, 0, 5, 5, 5}, false},
		{"LostUsers5days10back", args{"firstTestApp", 5, 10, 5}, []int{5, 5, 5, 0, 0}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countLostUsers(tt.args.appname, tt.args.days, tt.args.offset, tt.args.activity)
			if (err != nil) != tt.wantErr {
				t.Errorf("countLostUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countLostUsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countPayingUsers(t *testing.T) {
	type args struct {
		appname   string
		days      int
		offset    int
		threshold int
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{"PayingUsers1day", args{"firstTestApp", 1, 0, 5}, []int{34}, false},
		{"PayingUsers2days", args{"firstTestApp", 2, 0, 5}, []int{34, 34}, false},
		{"PayingUsers3days", args{"firstTestApp", 3, 0, 5}, []int{34, 34, 34}, false},
		{"PayingUsers4days", args{"firstTestApp", 4, 0, 5}, []int{34, 34, 34, 34}, false},
		{"PayingUsers5days", args{"firstTestApp", 5, 0, 5}, []int{34, 34, 34, 34, 34}, false},
		{"PayingUsers6days", args{"firstTestApp", 6, 0, 5}, []int{34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers7days", args{"firstTestApp", 7, 0, 5}, []int{34, 34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers8days", args{"firstTestApp", 8, 0, 5}, []int{34, 34, 34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers9days", args{"firstTestApp", 9, 0, 5}, []int{34, 34, 34, 34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers7days1back", args{"firstTestApp", 7, 1, 5}, []int{34, 34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers7days4back", args{"firstTestApp", 7, 4, 5}, []int{34, 34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers7days7back", args{"firstTestApp", 7, 7, 5}, []int{34, 34, 34, 34, 34, 34, 34}, false},
		{"PayingUsers5days10back", args{"firstTestApp", 5, 10, 5}, []int{37, 34, 34, 34, 34}, false},
		{"PayingUsers5days20back", args{"firstTestApp", 5, 20, 5}, []int{0, 0, 80, 40, 40}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countPayingUsers(tt.args.appname, tt.args.days, tt.args.offset, tt.args.threshold)
			if (err != nil) != tt.wantErr {
				t.Errorf("countPayingUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countPayingUsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

//NB: percentages are approximations only, they are rounded and don't have to add up to 100
//expected test results have been modified to fit results (cbb to find a better solution)
func Test_countSubLevels(t *testing.T) {
	type args struct {
		appname string
		days    int
		offset  int
	}
	tests := []struct {
		name    string
		args    args
		want    []map[string]int
		wantErr bool
	}{
		{"SubLevels1day", args{"firstTestApp", 1, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels2days", args{"firstTestApp", 2, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels3days", args{"firstTestApp", 3, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels4days", args{"firstTestApp", 4, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels5days", args{"firstTestApp", 5, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels6days", args{"firstTestApp", 6, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels7days", args{"firstTestApp", 7, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels8days", args{"firstTestApp", 8, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels7days1back", args{"firstTestApp", 7, 1}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels7days4back", args{"firstTestApp", 7, 4}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels7days7back", args{"firstTestApp", 7, 7}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels5days10back", args{"firstTestApp", 5, 10}, []map[string]int{
			{"free": 76, "bronze": 4, "silver": 9, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
			{"free": 78, "bronze": 4, "silver": 8, "gold": 10},
		}, false},
		{"SubLevels5days14back", args{"firstTestApp", 5, 14}, []map[string]int{
			{"free": 79, "bronze": 5, "silver": 5, "gold": 11},
			{"free": 81, "bronze": 5, "silver": 5, "gold": 9},
			{"free": 77, "bronze": 4, "silver": 8, "gold": 12},
			{"free": 79, "bronze": 4, "silver": 7, "gold": 11},
			{"free": 76, "bronze": 4, "silver": 9, "gold": 10},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countSubLevels(tt.args.appname, tt.args.days, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("countSubLevels() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countSubLevels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countSubNames(t *testing.T) {
	type args struct {
		appname string
		days    int
		offset  int
	}
	tests := []struct {
		name    string
		args    args
		want    []map[string]int
		wantErr bool
	}{
		{"SubNames1day", args{"firstTestApp", 1, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames2days", args{"firstTestApp", 2, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames3days", args{"firstTestApp", 3, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames4days", args{"firstTestApp", 4, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames5days", args{"firstTestApp", 5, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames6days", args{"firstTestApp", 6, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames7days", args{"firstTestApp", 7, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames8days", args{"firstTestApp", 8, 0}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames7days1back", args{"firstTestApp", 7, 1}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames7days4back", args{"firstTestApp", 7, 4}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames7days7back", args{"firstTestApp", 7, 7}, []map[string]int{
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames5days10back", args{"firstTestApp", 5, 10}, []map[string]int{
			{"free": 76, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
			{"free": 78, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
		{"SubNames5days14back", args{"firstTestApp", 5, 14}, []map[string]int{
			{"free": 79, "bronze": 5, "silver": 3, "gold": 3, "longSilver": 3, "longGold": 3, "freeGold": 5},
			{"free": 81, "bronze": 5, "silver": 2, "gold": 2, "longSilver": 2, "longGold": 2, "freeGold": 5},
			{"free": 77, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 4, "freeGold": 4},
			{"free": 79, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 4, "freeGold": 4},
			{"free": 76, "bronze": 4, "silver": 4, "gold": 4, "longSilver": 4, "longGold": 3, "freeGold": 3},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countSubNames(tt.args.appname, tt.args.days, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("countSubNames() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countSubNames() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countNewSubs(t *testing.T) {
	type args struct {
		appname string
		days    int
		offset  int
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{"NewSubs1day", args{"firstTestApp", 1, 0}, []int{0}, false},
		{"NewSubs2days", args{"firstTestApp", 2, 0}, []int{0, 0}, false},
		{"NewSubs3days", args{"firstTestApp", 3, 0}, []int{0, 0, 0}, false},
		{"NewSubs4days", args{"firstTestApp", 4, 0}, []int{0, 0, 0, 0}, false},
		{"NewSubs5days", args{"firstTestApp", 5, 0}, []int{0, 0, 0, 0, 0}, false},
		{"NewSubs6days", args{"firstTestApp", 6, 0}, []int{0, 0, 0, 0, 0, 0}, false},
		{"NewSubs7days", args{"firstTestApp", 7, 0}, []int{0, 0, 0, 0, 0, 0, 0}, false},
		{"NewSubs8days", args{"firstTestApp", 8, 0}, []int{0, 0, 0, 0, 0, 0, 0, 0}, false},
		{"NewSubs9days", args{"firstTestApp", 9, 0}, []int{0, 0, 0, 0, 0, 0, 0, 0, 0}, false},
		{"NewSubs7days1back", args{"firstTestApp", 7, 1}, []int{0, 0, 0, 0, 0, 0, 0}, false},
		{"NewSubs7days4back", args{"firstTestApp", 7, 4}, []int{0, 0, 0, 0, 0, 0, 0}, false},
		{"NewSubs7days7back", args{"firstTestApp", 7, 7}, []int{0, 0, 0, 0, 0, 0, 0}, false},
		{"NewSubs5days10back", args{"firstTestApp", 5, 10}, []int{5, 0, 0, 0, 0}, false},
		{"NewSubs5days14back", args{"firstTestApp", 5, 14}, []int{5, 10, 5, 10, 5}, false},
		{"NewSubs5days20back", args{"firstTestApp", 5, 20}, []int{0, 10, 5, 10, 5}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countNewSubs(tt.args.appname, tt.args.days, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("countNewSubs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countNewSubs() = %v, want %v", got, tt.want)
			}
		})
	}
}

//approximate equality check for floats by Adam Cardenas, at: https://gist.github.com/cevaris/bc331cbe970b03816c6b
var EPSILON = 0.00100000

func floatAlmostEquals(a, b float64) bool {
	if (a-b) < EPSILON && (b-a) < EPSILON {
		return true
	}
	return false
}

func Test_countRevenue(t *testing.T) {
	type args struct {
		appname string
		days    int
		offset  int
	}
	tests := []struct {
		name    string
		args    args
		want    []float64
		wantErr bool
	}{
		{"Rev1day", args{"firstTestApp", 1, 0}, []float64{3.75}, false},
		{"Rev2days", args{"firstTestApp", 2, 0}, []float64{3.75, 3.75}, false},
		{"Rev3days", args{"firstTestApp", 3, 0}, []float64{3.75, 3.75, 3.75}, false},
		{"Rev4days", args{"firstTestApp", 4, 0}, []float64{3.75, 3.75, 3.75, 3.75}, false},
		{"Rev5days", args{"firstTestApp", 5, 0}, []float64{3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev6days", args{"firstTestApp", 6, 0}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev7days", args{"firstTestApp", 7, 0}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev8days", args{"firstTestApp", 8, 0}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev9days", args{"firstTestApp", 9, 0}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev7days1back", args{"firstTestApp", 7, 1}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev7days4back", args{"firstTestApp", 7, 4}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev7days7back", args{"firstTestApp", 7, 7}, []float64{3.75, 3.75, 3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev5days10back", args{"firstTestApp", 5, 10}, []float64{3.75, 3.75, 3.75, 3.75, 3.75}, false},
		{"Rev5days14back", args{"firstTestApp", 5, 14}, []float64{1.65, 1.65, 2.7, 2.7, 3.75}, false},
		{"Rev5days20back", args{"firstTestApp", 5, 20}, []float64{0, 0, 2, 2, 2.75}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countRevenue(tt.args.appname, tt.args.days, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("countRevenue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for i, f := range got {
				if !floatAlmostEquals(f, tt.want[i]) {
					t.Errorf("countRevenue() = %v, want %v", got, tt.want)
					break
				}
			}
			// if !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("countRevenue() = %v, want %v", got, tt.want)
			// }
		})
	}
}

func Test_countRevenuePaying(t *testing.T) {
	type args struct {
		appname   string
		days      int
		offset    int
		threshold int
	}
	tests := []struct {
		name    string
		args    args
		want    []float64
		wantErr bool
	}{
		{"RevPaying1day", args{"firstTestApp", 1, 0, 5}, []float64{11.029}, false},
		{"RevPaying2days", args{"firstTestApp", 2, 0, 5}, []float64{11.029, 11.029}, false},
		{"RevPaying3days", args{"firstTestApp", 3, 0, 5}, []float64{11.029, 11.029, 11.029}, false},
		{"RevPaying4days", args{"firstTestApp", 4, 0, 5}, []float64{11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying5days", args{"firstTestApp", 5, 0, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying6days", args{"firstTestApp", 6, 0, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying7days", args{"firstTestApp", 7, 0, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying8days", args{"firstTestApp", 8, 0, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying9days", args{"firstTestApp", 9, 0, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying7days1back", args{"firstTestApp", 7, 1, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying7days4back", args{"firstTestApp", 7, 4, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying7days7back", args{"firstTestApp", 7, 7, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying5days10back", args{"firstTestApp", 5, 10, 5}, []float64{11.029, 11.029, 11.029, 11.029, 11.029}, false},
		{"RevPaying5days14back", args{"firstTestApp", 5, 14, 5}, []float64{10.312, 10.312, 11.25, 11.25, 11.029}, false},
		{"RevPaying5days20back", args{"firstTestApp", 5, 20, 5}, []float64{0, 0, 10, 10, 9.166}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countRevenuePaying(tt.args.appname, tt.args.days, tt.args.offset, tt.args.threshold)
			if (err != nil) != tt.wantErr {
				t.Errorf("countRevenuePaying() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for i, f := range got {
				if !floatAlmostEquals(f, tt.want[i]) {
					t.Errorf("countRevenuePaying() = %v, want %v", got, tt.want)
					break
				}
			}
			// if !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("countRevenuePaying() = %v, want %v", got, tt.want)
			// }
		})
	}
}

func Test_countTag(t *testing.T) {
	type args struct {
		appname string
		days    int
		offset  int
		tag     string
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{"Tag1day", args{"firstTestApp", 1, 0, "blackFriday16"}, []int{6}, false},
		{"Tag2days", args{"firstTestApp", 2, 0, "blackFriday16"}, []int{6, 6}, false},
		{"Tag3days", args{"firstTestApp", 3, 0, "blackFriday16"}, []int{6, 6, 6}, false},
		{"Tag4days", args{"firstTestApp", 4, 0, "blackFriday16"}, []int{6, 6, 6, 6}, false},
		{"Tag5days", args{"firstTestApp", 5, 0, "blackFriday16"}, []int{6, 6, 6, 6, 6}, false},
		{"Tag6days", args{"firstTestApp", 6, 0, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6}, false},
		{"Tag7days", args{"firstTestApp", 7, 0, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag8days", args{"firstTestApp", 8, 0, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag9days", args{"firstTestApp", 9, 0, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag28days", args{"firstTestApp", 28, 0, "blackFriday16"}, []int{0, 0, 0, 0, 0, 0, 0, 0, 13, 9, 8, 7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag7days1back", args{"firstTestApp", 7, 1, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag7days4back", args{"firstTestApp", 7, 4, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag7days7back", args{"firstTestApp", 7, 7, "blackFriday16"}, []int{6, 6, 6, 6, 6, 6, 6}, false},
		{"Tag7days14back", args{"firstTestApp", 7, 14, "blackFriday16"}, []int{0, 13, 9, 8, 7, 6, 6}, false},
		{"Tag5days10back", args{"firstTestApp", 5, 10, "blackFriday16"}, []int{6, 6, 6, 6, 6}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countTag(tt.args.appname, tt.args.days, tt.args.offset, tt.args.tag)
			if (err != nil) != tt.wantErr {
				t.Errorf("countTag() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countTag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countConversionLeves(t *testing.T) {
	type args struct {
		appname  string
		days     int
		offset   int
		interval int
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]map[string]int
		wantErr bool
	}{
		{"ConvLevels1day", args{"firstTestApp", 1, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels2days", args{"firstTestApp", 2, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels3days", args{"firstTestApp", 3, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels4days", args{"firstTestApp", 4, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels5days", args{"firstTestApp", 5, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels6days", args{"firstTestApp", 6, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels7days", args{"firstTestApp", 7, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels8days", args{"firstTestApp", 8, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels9days", args{"firstTestApp", 9, 0, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels7days1back", args{"firstTestApp", 7, 1, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels7days4back", args{"firstTestApp", 7, 4, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels7days7back", args{"firstTestApp", 7, 7, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels5days10back", args{"firstTestApp", 5, 10, 30}, map[string]map[string]int{
			"free":   {"none": 60, "free": 10, "bronze": 5, "silver": 10, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels5days14back", args{"firstTestApp", 5, 14, 30}, map[string]map[string]int{
			"free":   {"none": 58, "free": 10, "bronze": 5, "silver": 10, "gold": 14},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
		{"ConvLevels5days20back", args{"firstTestApp", 5, 20, 30}, map[string]map[string]int{
			"free":   {"none": 58, "free": 16, "bronze": 8, "silver": 4, "gold": 12},
			"bronze": {"none": 100},
			"silver": {"none": 100},
			"gold":   {"none": 100},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countConversionLevels(tt.args.appname, tt.args.days, tt.args.offset, tt.args.interval)
			if (err != nil) != tt.wantErr {
				t.Errorf("countConversions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countConversions() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_countConversionNames(t *testing.T) {
	type args struct {
		appname  string
		days     int
		offset   int
		interval int
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]map[string]int
		wantErr bool
	}{
		{"ConvNames1day", args{"firstTestApp", 1, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames2days", args{"firstTestApp", 2, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames3days", args{"firstTestApp", 3, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames4days", args{"firstTestApp", 4, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames5days", args{"firstTestApp", 5, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames6days", args{"firstTestApp", 6, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames7days", args{"firstTestApp", 7, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames8days", args{"firstTestApp", 8, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames9days", args{"firstTestApp", 9, 0, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames7days1back", args{"firstTestApp", 7, 1, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames7days4back", args{"firstTestApp", 7, 4, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames7days7back", args{"firstTestApp", 7, 7, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames5days10back", args{"firstTestApp", 5, 10, 30}, map[string]map[string]int{
			"free":       {"none": 60, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5},
			"bronze":     {"none": 100},
			"silver":     {"none": 100},
			"gold":       {"none": 100},
			"longSilver": {"none": 100},
			"longGold":   {"none": 100},
			"freeGold":   {"none": 100},
		}, false},
		{"ConvNames5days14back", args{"firstTestApp", 5, 14, 30}, map[string]map[string]int{
			"free":         {"none": 58, "free": 10, "bronze": 5, "silver": 5, "gold": 5, "longGold": 3, "freeGold": 3, "longSilver": 5, "lifetimeGold": 1},
			"bronze":       {"none": 100},
			"silver":       {"none": 100},
			"gold":         {"none": 100},
			"longSilver":   {"none": 100},
			"longGold":     {"none": 100},
			"freeGold":     {"none": 100},
			"lifetimeGold": {"none": 100},
		}, false},
		{"ConvNames5days20back", args{"firstTestApp", 5, 20, 30}, map[string]map[string]int{
			"free":         {"none": 58, "free": 16, "bronze": 8, "silver": 4, "gold": 4, "lifetimeGold": 4, "freeGold": 4},
			"bronze":       {"none": 100},
			"silver":       {"none": 100},
			"gold":         {"none": 100},
			"lifetimeGold": {"none": 100},
			"freeGold":     {"none": 100},
		}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := countConversionNames(tt.args.appname, tt.args.days, tt.args.offset, tt.args.interval)
			if (err != nil) != tt.wantErr {
				t.Errorf("countConversionNames() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("countConversionNames() = %v, want %v", got, tt.want)
			}
		})
	}
}
